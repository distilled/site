const _ = require('lodash'),
      fs = require('fs-extra'),
      path = require('path'),
      less = require('less'),
      Handlebars = require('handlebars'),
      imageMagick = require('imagemagick'),
      imagemin = require('imagemin'),
      imagemin_optipng = require('imagemin-optipng'),
      browserify = require('browserify'),
      dormouse = require('@latinfor/dormouse'),
      marked = require('marked');

const BUILD = {
    time : Date.now() + ''
};

const SOURCES = {
    JS : {
        distilled : {
            path : require.resolve('@latinfor/distilled'),
            out : 'distilled.js',
            name : 'Distilled'
        },
        codemirror : {
            path : require.resolve('bundledmirror'),
            out : 'codemirror.js',
            name : 'CodeMirror'
        },
        example : {
            path : require.resolve('example'),
            out : 'example.js',
            name : 'example'
        }
    },
    CSS : {
        codemirror : {
            path : require.resolve('codemirror/lib/codemirror.css'),
            out : 'codemirror.css'
        },
        codetheme : {
            path : require.resolve('codemirror/theme/zenburn.css'),
            out : 'zenburn.css'
        },
        codegutters : {
            path : require.resolve('codemirror/addon/fold/foldgutter.css'),
            out : 'foldgutter.css'
        },
        main : {
            path : require.resolve('./styles/main.less'),
            out : 'main' + BUILD.time + '.css'
        }
    },
    HTML : {
        main : {
            path : require.resolve('./pages/index.hbs'),
            out : 'index.html'
        }
    },
    FONTS : {
        quicksand : {
            path : require.resolve('./fonts/quicksand/Quicksand-Regular.ttf'),
            out : 'quicksand-regular.ttf'
        },
        ubuntu: {
            path: require.resolve('./fonts/ubuntu/Ubuntu-R.ttf'),
            out: 'ubuntu-regular.ttf'
        }
    },
    IMAGES : {
        distilled_large : {
            path : require.resolve('./images/distilled.png'),
            out : 'distilled-large.png',
            width : 600
        },
        distilled : {
            path : require.resolve('./images/distilled.png'),
            out: 'distilled-small.png',
            width : 300
        },
        gitlab : {
            path : require.resolve('./images/gitlab.png'),
            out : 'gitlab.png',
            width : 200
        },
        banner : {
            path: require.resolve('./images/distilled-banner.png'),
            out: 'banner.png',
            width: 1200
        },

        /* Logo list */
        logo_196: {
            path: require.resolve('./images/distilled-logo.png'),
            out: 'logo-196.png',
            width: 196
        },
        logo_96: {
            path: require.resolve('./images/distilled-logo.png'),
            out: 'logo-96.png',
            width: 96,
        },
        logo_32: {
            path: require.resolve('./images/distilled-logo.png'),
            out: 'logo-32.png',
            width: 32
        },
        logo_16: {
            path: require.resolve('./images/distilled-logo.png'),
            out: 'logo-16.png',
            width: 16
        }
    },
    DOCS: (function () {
        var base = path.dirname(require.resolve('@latinfor/distilled'));

        return {
            "getting started": {
                path: path.resolve(base, '../tests/distilled/overview.js'),
                template: require.resolve('./pages/doc.hbs'),
                out: 'introduction/index.html'
            },

            test: {
                path: path.resolve(base, '../tests/distilled/test.js'),
                template: require.resolve('./pages/doc.hbs'),
                out: 'docs/test/index.html'
            },

            then: {
                path: path.resolve(base, '../tests/distilled/then.js'),
                template: require.resolve('./pages/doc.hbs'),
                out: 'docs/then/index.html'
            },

            assert: {
                path: path.resolve(base, '../tests/distilled/assertions.js'),
                template: require.resolve('./pages/doc.hbs'),
                out: 'docs/assert/index.html'
            },

            reporter : {
                path: path.resolve(base, '../tests/distilled/callback.js'),
                template: require.resolve('./pages/doc.hbs'),
                out: 'docs/reporter/index.html'
            },
        };
    }())

};

const OUT = {
    js : path.resolve(__dirname, '../public/src'),
    css : path.resolve(__dirname, '../public/styles'),
    html : path.resolve(__dirname, '../public'),
    fonts : path.resolve(__dirname, '../public/fonts'),
    images : path.resolve(__dirname, '../public/images')
};

//------------------ACTIONS--------------------------

async function compileScript (source) {

    //@todo: should also derequire? (https://www.npmjs.com/package/derequire)
    return new Promise(function (res, rej) {
        browserify(source.path, {
            standalone : source.name
        }).bundle(function (err, buffer) {
            if (err) { rej(err); } else { res(buffer); }
        });
    });

    //return fs.writeFile(path.resolve(__dirname, OUT.js, options.output), buffer);
}

async function compileStyle (source, name) {
    var buffer = await fs.readFile(source.path, 'utf8');

    return (
        await less.render(buffer)
    ).css;
}

async function compilePage (source) {
    var file = await fs.readFile(source.path, 'binary');
    var page = Handlebars.compile(file);
    var compiled = page({
        build : BUILD,
        scripts : {
            main: [
                'function fibonacci (index) {',
                '  if (index <= 1) return 1;',
                '',
                '  return fibonacci(index - 1) + fibonacci(index - 2);',
                '}',
                '',
                'var suite = new Distilled();',
                'suite.test(\'fibonacci\', function () {',
                '  this.test(\'0\', fibonacci(0) === 1);',
                '  this.test(\'1\', fibonacci(1) === 1);',
                '  this.test(\'2\', fibonacci(2) === 2);',
                '  this.test(\'3\', fibonacci(3) === 3);',
                '  this.test(\'5\', fibonacci(5) === 8);',
                '',
                '  this.test(\'Error handling\', function () {',
                '    this.test(\'-1\', fibonacci(-1) === -1);',
                '  });',
                '});'
            ].join('\n')
        }
    });

    return compiled;
}

async function compileImage(source) {

    await new Promise(function (res, rej) {
        imageMagick.resize({
            srcPath: source.path,
            dstPath: path.resolve(OUT.images, source.out),
            width: source.width
        }, function (err) {
            if (err) { rej(err); } else { res(); }
        });
    });

    await new Promise(function (res, rej) {
        imagemin(
            [path.resolve(OUT.images, source.out)],
            path.resolve(OUT.images),
            {
                plugins : [
                    imagemin_optipng()
                ]
            });
    });

}

async function convertTests(source, title) {

    let graph = await dormouse(source.path, {
        type: 'glob'
    });

    let start = graph.start.links[0];

    /* Always skip the first section header, it's just repeating the title. */
    graph[start].properties.skip = true;

    var sections = [];
    var cur_section = null;
    var blocks = (function parse (raw_node) {

        if (!raw_node) { return null; } /* Finished */

        /* Make a new section whenever you encounter a section block.
         * Blocks that are not part of a section should just get discarded?
         * Or they should be appended to the header, but I'm not worrying that right now.
         * There's no reason, since none of the documentation has any blocks like this. */
        if (raw_node.properties.section) {
            cur_section = {
                visible: !raw_node.properties.skip,
                title: raw_node.properties.section,
                blocks: []
            };

            sections.push(cur_section);
            return parse(graph[raw_node.next]);
        }

        let block = {
            content: marked(raw_node.text),
            type: 'text'
        };

        if (raw_node.properties.type === 'source') {
            block.type = 'source';
            block.isSource = true;

            let lines = raw_node.text.split('\n');
            let title = lines[0].match(/['"](.*)['"]/)[1];
            let content = lines
                .slice(1, -1)
                .map((line) => {
                    return line.slice(4); /* Remove first tab -- hack, but whatever. */
                }).join('\n');

            block.title = title;
            block.content = content;
        }

        cur_section.blocks.push(block);

        return parse(graph[raw_node.next]);

    }(graph[start]));

    var file = await fs.readFile(source.template, 'binary');
    var page = Handlebars.compile(file);
    var compiled = page({
        build: BUILD,
        title: title,
        sections: sections
    });

    return compiled;
}

async function run () {
    await fs.ensureDir(OUT.js);
    _.each(SOURCES.JS, function (source) {
        compileScript(source).then(function (buffer) {
            fs.writeFile(path.resolve(OUT.js, source.out), buffer);
        });
    });

    await fs.ensureDir(OUT.css);
    _.each(SOURCES.CSS, function (source) {
        compileStyle(source).then(function (buffer) {
            fs.writeFile(path.resolve(OUT.css, source.out), buffer);
        });
    });

    await fs.ensureDir(OUT.html);
    _.each(SOURCES.HTML, function (source) {
        compilePage(source).then(function (buffer) {
            fs.writeFile(path.resolve(OUT.html, source.out), buffer);
        });
    });

    await fs.ensureDir(OUT.fonts);
    _.each(SOURCES.FONTS, function (source) {
        console.log(source.out);
        fs.copy(source.path, path.resolve(OUT.fonts, source.out));
    });

    await fs.ensureDir(OUT.images);
    _.each(SOURCES.IMAGES, function (source) {
        compileImage(source);
    });

    /* Push out the rest of the HTML pages for docs/etc... */
    _.each(SOURCES.DOCS, async function (source, title) {
        await fs.ensureDir(path.dirname(path.resolve(OUT.html, source.out)));

        console.log(path.dirname(path.resolve(OUT.html, source.out)));

        convertTests(source, title)
            .then((buffer) => {
                fs.writeFile(path.resolve(OUT.html, source.out), buffer);
            })
            .catch(err => console.log(err));
    });
}

run();
